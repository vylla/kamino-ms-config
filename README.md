# Kamino ms-config #

ms-config is a Spring cloud config server tied to `kamino-config-data` repository to provide cloud configuration to services, applications and middleware layers.

## Configuration of ms-config ##

- First generate a ssh rsa key for the communication. Don't use a passphrase.

```bash
    ssh-keygen
```

-  Config your local helm/kubernetes repo to use `kamino-helm-charts`. 
the config micro service uses the ms-config helm chart with a set of default values. 


- Create a values.yml file where the private key is stored 
```yaml
git:
   privateKey: |
     -----BEGIN RSA PRIVATE KEY-----
     PUT YOUR PRIVATE KEY HERE
     -----END RSA PRIVATE KEY-----
```

- If your config data repository is not `kamino-config-data` you need to provide configuration in the values.yml

```yaml
git:
  hostKeyAlgorithm: ssh-rsa
  hostKey: your known_hosts key for the repository domain (public key of bitbucket, github or gitlab)
  repository: git@domain.com:project/some-config-data.git
``` 

- Then deploy ms-config with your custom `values.yml` file.

```bash
$ helm upgrade -i ms-config-myproject ms-config -f /path/to/values.yml --namespace=dev
```

## Run it standalone with docker

- Go to the Mahisoft container registry https://console.cloud.google.com

- Locate the ms-config docker image `ms-config-service`

- Run it with the following environment variables:

```bash
    CONFIG_DATA_URI="git@domain.com:project/some-config-data.git"
    CONFIG_HOST_KEY= "the known host key of the repository domain"
    CONFIG_HOST_KEY_ALGORITHM="ssh-rsa"
    CONFIG_PRIVATE_KEY="private key generated"
```

## References 
https://cloud.spring.io/spring-cloud-config/
https://daemonza.github.io/2017/02/20/using-helm-to-deploy-to-kubernetes/


## Who do I talk to? 

Andreth Salazar

Orlando Vargas

Saul Hernandez

Julio Naranjo
